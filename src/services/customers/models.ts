export interface IFeedbackModel {
  id: number,
  text: string,
}

export interface ICustomerModel {
  id: number,
  name: string,
  feedbacks: IFeedbackModel[],
}
