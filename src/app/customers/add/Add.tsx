import React, { FunctionComponent } from 'react'

import {
  useAddState,
  useSwitchState,
} from './useAddState'

import { Button, Form, Input, Layout, Title } from './styled'

export interface IAddProperties {
  onAdd: (name: string) => void
}

export const Add: FunctionComponent<IAddProperties> = ({ onAdd }) => {
  const { isAdding, turnOn, turnOff } = useSwitchState()
  const { name, onChange, onSubmit } = useAddState({ onAdd, onAction: turnOff })

  if (!isAdding) {
    return (
      <Layout onClick={turnOn}>
        <Title>Add</Title>
      </Layout>
    )
  }

  return (
    <Layout>
      <Title>Add</Title>
      <Form onSubmit={onSubmit}>
        <Input value={name} onChange={onChange} />
        <Button type='submit'>submit</Button>
        <Button type='button' onClick={turnOff}>cancel</Button>
      </Form>
    </Layout>
  )
}
