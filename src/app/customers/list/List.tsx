import React, { FunctionComponent } from 'react'

import { ICustomerModel } from '@/services/customers'

import { ListLayout } from '@/components/layout'
import { HeaderH2 } from '@/components/typography'

import { Layout } from './styled'

import { Add } from '../add'
import { Item } from './item'

interface IListProperties {
  items: ICustomerModel[],
  selectedId: number,
  onSelect: (id: number) => void
}

export const List: FunctionComponent<IListProperties> = ({
  items,
  selectedId,
  onSelect,
  children,
}) => {
  const onClick = (id: number) => (): void => onSelect(id)
  
  return (
    <Layout>
      <HeaderH2>Customers</HeaderH2>

      {children}

      <ListLayout>
        {items.map(item => (
          <Item
            key={item.id}
            item={item}
            isSelected={item.id === selectedId}
            onClick={onClick(item.id)}
          />
        ))}
      </ListLayout>
    </Layout>
  )
}
