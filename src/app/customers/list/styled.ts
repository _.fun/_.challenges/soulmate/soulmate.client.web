import styled, { css } from '@/styled-components'

export const Layout = styled.div`
  ${({ theme: { spaces } }) => css`
    grid-area: list;

    padding: 0 ${spaces.x1};

    display: grid;
    grid-auto-flow: row;
    grid-gap: ${spaces.x2};

    grid-auto-rows: min-content;
  `}
`
