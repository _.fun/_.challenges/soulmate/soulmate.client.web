import React, { FunctionComponent } from 'react'

import { ICustomerModel } from '@/services/customers'

import { ItemLayout } from '@/components/layout'

interface IItemProperties {
  item: ICustomerModel
  isSelected: boolean
  onClick: () => void
}

export const Item: FunctionComponent<IItemProperties> = ({ item, isSelected, onClick }) => (
  <ItemLayout
    isSelected={isSelected}
    onClick={onClick}
  >
    {item.name} ({item.feedbacks.length})
  </ItemLayout>
)
