import { useHistory, useParams } from 'react-router'

interface IParameters {
  customerId?: string
}

interface IRouteState {
  customerId?: number
  navigateToCustomer: (id: number) => void
  navigateToDefault: () => void
}

export const useCustomersRoute = (): IRouteState => {
  const { customerId } = useParams<IParameters>()
  const history = useHistory()

  const navigateToCustomer = (id: number): void => history.push(`/${id}`)
  const navigateToDefault = (): void => history.push('/')

  return {
    customerId: parseFloat(customerId),
    navigateToCustomer,
    navigateToDefault,
  }
}
