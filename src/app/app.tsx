import React, { FunctionComponent } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import { Customers } from './customers'

import { Error404 } from './error'

export const App: FunctionComponent = () => (
  <Switch>
    <Route path='/:customerId?' exact component={Customers} />
    <Route path='/*' component={Error404} status={404} />
  </Switch>
)
