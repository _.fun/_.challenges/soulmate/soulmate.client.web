import * as styledComponents from 'styled-components'

import { ITheme, theme } from './theme'

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider
} = styledComponents as styledComponents.ThemedStyledComponentsModule<ITheme>

export { css, theme, createGlobalStyle, keyframes, ThemeProvider }
export default styled