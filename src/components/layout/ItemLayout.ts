import styled, { css } from '@/styled-components'

interface IItemProperties {
  isSelected?: boolean
}

export const ItemLayout = styled.div<IItemProperties>`
  ${({ isSelected, onClick, theme: { colors, values, spaces } }) => css`
    padding: ${spaces.x1};
    border: ${values.itemBorder} ${colors.itemBorder};
    border-radius: ${values.itemBorderRadius};

    ${isSelected && css`
      background-color: ${colors.itemSelected};
    `}

    ${onClick && css`
      &:hover {
        cursor: pointer;
        background-color: ${colors.itemHover};
      }
    `}
  `}
`
