const webpack = require('webpack')
const merge = require('webpack-merge')
const DeadCodePlugin = require('webpack-deadcode-plugin')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    contentBase: common.output.path,
    disableHostCheck: true,
    historyApiFallback: true,
    port: 3000,
  },
  devtool: 'cheap-module-source-map',
  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: 'development',
    }),
    new DeadCodePlugin({
      patterns: ['src/**/*.(ts|tsx|css)'],
      exclude: ['**/*.(stories|story|spec).(ts|tsx)'],
    }),
  ],
  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
  },
  output: {
    filename: 'main.js',
  },
})
